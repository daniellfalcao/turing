package com.falcon.turing.location

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.location.Location
import android.os.Looper
import com.falcon.turing.core.utils.isPermissionToAccessLocationGranted
import com.google.android.gms.location.*
import java.util.concurrent.atomic.AtomicBoolean

open class LocationController {

    var priority: Int = LocationRequest.PRIORITY_LOW_POWER
        @SuppressLint("MissingPermission")
        set(value) {

            field = value

            locationUpdateCallback?.let { callback ->
                locationRequest.priority = value
                fusedLocationProviderClient?.requestLocationUpdates(locationRequest, callback, Looper.myLooper())
            }
        }

    var interval: Long = 1000L
        @SuppressLint("MissingPermission")
        set(value) {

            field = value

            locationUpdateCallback?.let { callback ->
                locationRequest.interval = value
                fusedLocationProviderClient?.requestLocationUpdates(locationRequest, callback, Looper.myLooper())
            }
        }

    var fatestInterval: Long = 1000L
        @SuppressLint("MissingPermission")
        set(value) {

            field = value

            locationUpdateCallback?.let { callback ->
                locationRequest.fastestInterval = value
                fusedLocationProviderClient?.requestLocationUpdates(locationRequest, callback, Looper.myLooper())
            }
        }

    var lastLocation: Location? = null

    private var locationUpdateCallback: LocationCallback? = null
    private var locationRequest: LocationRequest = LocationRequest()

    private var isLocationAvailable: AtomicBoolean = AtomicBoolean(false)

    private var _onLocationAvailable: MutableList<() -> Unit> = mutableListOf()
    fun addOnLocationAvailable(event: () -> Unit) = apply { _onLocationAvailable.add(event) }

    private var _onLocationChanged: MutableList<(location: Location) -> Unit> = mutableListOf()
    fun addOnLocationChanged(event: (location: Location) -> Unit) = apply { _onLocationChanged.add(event) }

    private var fusedLocationProviderClient: FusedLocationProviderClient? = null

    fun startLocationService(activity: Activity) {
        startLocationService(activity as Context)
    }

    @SuppressLint("MissingPermission")
    fun startLocationService(context: Context) {

        if (!context.isPermissionToAccessLocationGranted()) return

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context).apply {

            locationRequest.apply {
                priority = this@LocationController.priority
                interval = this@LocationController.interval
                fastestInterval = this@LocationController.fatestInterval
            }

            locationUpdateCallback = object : LocationCallback() {

                override fun onLocationResult(locationResult: LocationResult?) {
                    super.onLocationResult(locationResult)

                    if (locationResult == null || locationResult.lastLocation == null) return

                    locationResult.lastLocation.also { location ->

                        this@LocationController.lastLocation = location
                        _onLocationChanged.forEach { it(location) }
                    }
                }

                override fun onLocationAvailability(locationAvailability: LocationAvailability?) {
                    super.onLocationAvailability(locationAvailability)

                    if (locationAvailability != null
                        && locationAvailability.isLocationAvailable
                        && isLocationAvailable.compareAndSet(false, true)
                    ) {

                        _onLocationAvailable.forEach { it() }
                    }
                }
            }

            requestLocationUpdates(locationRequest, locationUpdateCallback, Looper.myLooper())
        }
    }

    fun cancelLocationService() {

        fusedLocationProviderClient?.removeLocationUpdates(locationUpdateCallback)
        locationUpdateCallback = null
    }

}