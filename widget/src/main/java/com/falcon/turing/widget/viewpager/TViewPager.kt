package com.falcon.turing.widget.viewpager

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import android.util.AttributeSet

class TViewPager : androidx.viewpager.widget.ViewPager {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    private var onPageScrollStateChanged: ((state: Int) -> Unit)? = null
    private var onPageScrolled: ((position: Int, positionOffset: Float, positionOffsetPixels: Int) -> Unit)? = null
    private var onPageSelected: ((position: Int) -> Unit)? = null

    init {

        setupPageChangeListener()
    }

    fun setOnPageScrollStateChangedListener(listener: (state: Int) -> Unit) {
        onPageScrollStateChanged = listener
    }

    fun setOnPageScrolledListener(listener: (position: Int, positionOffset: Float, positionOffsetPixels: Int) -> Unit) {
        onPageScrolled = listener
    }

    fun setOnPageSelectedListener(listener: (position: Int) -> Unit) {
        onPageSelected = listener
    }

    private fun setupPageChangeListener() {

        addOnPageChangeListener(object : androidx.viewpager.widget.ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
                onPageScrollStateChanged?.invoke(state)
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                onPageScrolled?.invoke(position, positionOffset, positionOffsetPixels)
            }

            override fun onPageSelected(position: Int) {
                onPageSelected?.invoke(position)
            }
        })
    }

    class TStatePagerAdapter(val context: Context, fragmentManager: androidx.fragment.app.FragmentManager) : androidx.fragment.app.FragmentStatePagerAdapter(fragmentManager) {

        private var mFragments = ArrayList<Pair<String?, androidx.fragment.app.Fragment>>()
        private var mShowTitle = false

        fun setFragments(fragments: ArrayList<Pair<String?, androidx.fragment.app.Fragment>>, showTitle: Boolean = true) {

            mFragments = fragments
            mShowTitle = showTitle

            notifyDataSetChanged()
        }

        override fun getItem(position: Int): androidx.fragment.app.Fragment {
            return mFragments[position].second
        }

        override fun getCount(): Int {
            return mFragments.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return if (mShowTitle) mFragments[position].first else null
        }
    }

    class TPagerAdapter(val context: Context, fragmentManager: androidx.fragment.app.FragmentManager) : androidx.fragment.app.FragmentPagerAdapter(fragmentManager) {

        private var mFragments = ArrayList<Pair<String?, androidx.fragment.app.Fragment>>()
        private var mShowTitle = false

        fun setFragments(fragments: ArrayList<Pair<String?, androidx.fragment.app.Fragment>>, showTitle: Boolean = true) {

            mFragments = fragments
            mShowTitle = showTitle

            notifyDataSetChanged()
        }

        override fun getItem(position: Int): androidx.fragment.app.Fragment {
            return mFragments[position].second
        }

        override fun getCount(): Int {
            return mFragments.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return if (mShowTitle) mFragments[position].first else null
        }
    }


}