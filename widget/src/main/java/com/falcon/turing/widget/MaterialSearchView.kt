package com.falcon.turing.widget

@Deprecated("em refatoracao")
class MaterialSearchView /*: ConstraintLayout*/ {

//    // Callbacks
//    private var mTextChangeListener: android.text.TextWatcher? = null
//    private var mAnimator: Animator? = null
//    private var mIconified = true
//
//    private val textWatcher = TextWatcher().apply {
//
//        onTextChanged { s: CharSequence?, _, _, _ ->
//
//            val query = if (enableTrim) {
//                s?.toString()?.trim() ?: ""
//            } else {
//                s?.toString() ?: ""
//            }
//
//            onSearchText(query)
//
//            if (s?.length == 0) {
//                cancelButton.setInvisible()
//            } else {
//                cancelButton.setVisible()
//            }
//        }
//    }
//
//    private var onSearchOpen: () -> Unit = {}
//    private var onSearchClose: () -> Unit = {}
//    private var onSearchCancelled: () -> Unit = {}
//    private var onSearchText: (query: String) -> Unit = {}
//    private var onFocusChanged: (hasFocus: Boolean) -> Unit = {}
//
//    // Configurations
//
//    var activity: WeakReference<Activity>? = null
//    var menuItemID = -1
//
//    var enableTrim = true
//    var allowAnimation: Boolean = true
//
//    // Values
//    var hint: String = ""
//        set(value) {
//            field = value
//            queryText?.hint = value
//        }
//
//    var query: String = ""
//        get() = if (enableTrim) {
//            queryText?.text.toString().trim()
//        } else {
//            queryText?.toString() ?: ""
//        }
//
//    constructor(context: Context?) : this(context, null)
//    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
//    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
//
//        View.inflate(context, R.layout.material_search_view, this)
//
//        with(backButton) {
//
//            setOnClickListener {
//                hide()
//                onSearchClose()
//            }
//
//            backButton.isEnabled = true
//        }
//
//        with(cancelButton) {
//
//            setOnClickListener {
//                this@MaterialSearchView.queryText?.setText("", TextView.BufferType.EDITABLE)
//                onSearchCancelled()
//            }
//        }
//
//        with(queryText){
//            setOnFocusChangeListener { _, hasFocus ->
//                onFocusChanged(hasFocus)
//            }
//        }
//
//        mTextChangeListener = textWatcher.watcher
//    }
//
//    fun show() {
//
//        backButton?.isEnabled = true
//
//        queryText?.setText("", TextView.BufferType.EDITABLE)
//
//        enableTextChangeListener(true)
//
//        if (allowAnimation && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//
//            val activity = activity?.get() ?: throw IllegalStateException("Activity must be not null")
//
//            if (menuItemID == -1) {
//                throw IllegalStateException("menuItemID has a invalid item ID")
//            }
//
//            activity.findViewById<View>(menuItemID)?.let { createCircularReveal(it, true) }
//
//        } else {
//            setVisible()
//        }
//
//        mIconified = false
//    }
//
//    fun hide() {
//
//        backButton?.isEnabled = false
//
//        enableTextChangeListener(false)
//
//        if (allowAnimation && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//
//            val activity = activity?.get() ?: throw IllegalStateException("Activity must be not null")
//
//            if (menuItemID == -1) {
//                throw IllegalStateException("menuItemID has a invalid item ID")
//            }
//
//            activity.findViewById<View>(menuItemID)?.let { createCircularReveal(it, false) }
//
//        } else {
//            setInvisible()
//        }
//
//        mIconified = true
//    }
//
//    /***
//     * Checa se a searchView está encolhida.
//     */
//    fun isIconified() = mIconified
//
//    /***
//     * Força a search view a se encolher se ela estiver sendo exibida.
//     */
//    fun forceIconified() {
//        if (!mIconified) {
//
//            backButton.isEnabled = false
//            queryText.hint = ""
//
//            visibility = View.INVISIBLE
//            onSearchClose()
//
//            mIconified = true
//        }
//    }
//
//    /***
//     * Animate the searchView transitions states
//     */
//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
//    private fun createCircularReveal(view: View, isRevealing: Boolean) {
//
//        val location = IntArray(2)
//        view.getLocationInWindow(location)
//
//        val centerX = location[0] + (view.width / 2)
//        val centerY = location[1]
//
//        val startRadius: Float
//        val endRadius: Float
//
//        if (isRevealing) {
//            startRadius = 0F
//            endRadius = width.times(1.5F)
//        } else {
//            startRadius = width.times(1.5F)
//            endRadius = 0F
//        }
//
//        mAnimator = ViewAnimationUtils.createCircularReveal(this, centerX, centerY, startRadius, endRadius)
//        mAnimator?.interpolator = DecelerateInterpolator()
//        mAnimator?.duration = 400
//        mAnimator?.addListener(object : Animator.AnimatorListener {
//
//            override fun onAnimationEnd(animation: Animator?) {
//                if (isRevealing) {
//                    queryText?.requestFocus()
//                    queryText?.showKeyboard()
//                } else {
//                    queryText?.hideKeyboard()
//                    setInvisible()
//                }
//            }
//
//            override fun onAnimationStart(animation: Animator?) {
//                if (isRevealing) {
//                    setVisible()
//                }
//            }
//
//            override fun onAnimationRepeat(animation: Animator?) {}
//            override fun onAnimationCancel(animation: Animator?) {}
//
//        })
//
//        mAnimator?.start()
//
//    }
//
//    fun setOnSearchOpened(onSearchOpenClicked: () -> Unit) {
//        this.onSearchOpen = onSearchOpenClicked
//    }
//
//    fun setOnSearchClosed(onSearchCloseClicked: () -> Unit) {
//        this.onSearchClose = onSearchCloseClicked
//    }
//
//    fun setOnSearchCancelled(onSearchCancelledClicked: () -> Unit) {
//        onSearchCancelled = onSearchCancelledClicked
//    }
//
//    fun setOnSearchTextChanged(onTextChange: (query: String) -> Unit) {
//        onSearchText = onTextChange
//    }
//
//    fun removeAllChangeListeners() {
//        onSearchCancelled = { }
//        onSearchClose = { }
//        onSearchOpen = { }
//        onSearchText = { }
//    }
//
//    fun setOnFocusChanged(event: (hasFocus: Boolean) -> Unit ){
//        onFocusChanged = event
//    }
//
//    override fun clearFocus() {
//        super.clearFocus()
//        queryText.clearFocus()
//    }
//
//    private fun enableTextChangeListener(enable: Boolean) {
//        if (enable) {
//            queryText.addTextChangedListener(mTextChangeListener)
//        } else {
//            queryText.removeTextChangedListener(mTextChangeListener)
//        }
//    }

}
