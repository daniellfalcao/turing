package com.falcon.turing.widget.recyclerview

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.falcon.turing.core.utils.setGone
import com.falcon.turing.core.utils.setVisible
import com.falcon.turing.widget.recyclerview.RecyclerViewState.*

enum class RecyclerViewState { IDLE, LOADING, PAGGING, EMPTY, CONNECTION_ERROR }

open class StateRecyclerView : RecyclerView {

    // Configuracoes do estado LOADING
    private var allowLoadingState = false
    private var loadingView: View? = null

    // Configuracoes do estado PAGGING
    private var allowPaggingState = false
    private var paggingView: View? = null

    // Configuracoes do estado EMPTY
    private var allowEmptyState = false
    private var itemsWhenEmpty: Int = 0
    private var emptyView: View? = null

    // Configuracoes do estado CONNECTION_ERROR
    private var allowConnectionErrorState = false
    private var connectionView: View? = null
    private var buttonConnection: View? = null
    private var _onTryAgain: () -> Unit = { }

    private var _onStateChanged: (RecyclerViewState, RecyclerViewState) -> Unit = { _, _ -> }
    private var forceState = false

    var state: RecyclerViewState = IDLE
        private set

    private val dataObserver = object : RecyclerView.AdapterDataObserver() {

        override fun onChanged() {
            super.onChanged()
            updateState(state, forceState)
        }

        override fun onItemRangeChanged(positionStart: Int, itemCount: Int) {
            super.onItemRangeChanged(positionStart, itemCount)
            updateState(state, forceState)
        }

        override fun onItemRangeChanged(positionStart: Int, itemCount: Int, payload: Any?) {
            super.onItemRangeChanged(positionStart, itemCount, payload)
            updateState(state, forceState)
        }

        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            super.onItemRangeInserted(positionStart, itemCount)
            updateState(state, forceState)
        }

        override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
            super.onItemRangeMoved(fromPosition, toPosition, itemCount)
            updateState(state, forceState)
        }

        override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
            super.onItemRangeRemoved(positionStart, itemCount)
            updateState(state, forceState)
        }

    }

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle)

    override fun setAdapter(adapter: Adapter<*>?) {

        try {
            adapter?.unregisterAdapterDataObserver(dataObserver)
        } catch (e: Exception) {
        }

        adapter?.registerAdapterDataObserver(dataObserver)

        super.setAdapter(adapter)

        updateState(state, forceState)
    }

    fun setOnStateChanged(event: (lastState: RecyclerViewState, currentState: RecyclerViewState) -> Unit) = apply { _onStateChanged = event }

    open fun setupLoadingState(loadingView: View) {
        this.loadingView = loadingView
        allowLoadingState = true
    }

    open fun setupPaggingState(paggingView: View) {
        this.paggingView = paggingView
        allowPaggingState = true
    }

    open fun setupEmptyState(emptyView: View, itemsWhenEmpty: Int) {
        this.emptyView = emptyView
        this.itemsWhenEmpty = itemsWhenEmpty
        allowEmptyState = true
    }

    open fun setupConnectionErrorState(connectionView: View, buttonTryAgain: View) {

        this.connectionView = connectionView
        this.buttonConnection = buttonTryAgain

        buttonConnection?.setOnClickListener { _onTryAgain() }

        allowConnectionErrorState = true
    }

    open fun updateState(state: RecyclerViewState, force: Boolean = false) {

        forceState = force
        _onStateChanged(this.state, state)

        val adapterItemCount = adapter?.itemCount ?: 0

        // reseta os estados das views
        loadingView.setGone()
        paggingView.setGone()
        emptyView.setGone()
        connectionView.setGone()

        this.state = state

        when (state) {
            IDLE -> {
            }// faz nada, os estados ja foram resetados
            LOADING -> {

                if (adapterItemCount != 0) {

                    if (force) loadingView.setVisible()
                    loadingView.setGone()

                } else {
                    loadingView.setVisible()
                }

            }
            PAGGING -> {
                paggingView.setVisible()
            }
            EMPTY -> {
                if (adapterItemCount <= itemsWhenEmpty) emptyView.setVisible()
            }
            CONNECTION_ERROR -> {
                if (adapterItemCount == 0) connectionView.setVisible()
            }
        }
    }
}
