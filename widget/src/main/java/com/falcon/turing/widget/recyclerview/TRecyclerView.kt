package com.falcon.turing.widget.recyclerview


import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.falcon.turing.widget.listener.PaginationScrollListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.atomic.AtomicBoolean

open class TRecyclerView : StateRecyclerView {

    private val scrollListener = PaginationScrollListener()

    private var _onLoadMore: (totalItemCount: Int) -> Unit = { }
    private var _onScroll: () -> Unit = { }

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle)

    override fun setAdapter(adapter: Adapter<*>?) {

        if (adapter !is TAdapter<*, *>) {
            throw IllegalArgumentException("This RecyclerView is only used with TAdapter")
        }

        super.setAdapter(adapter)
    }

    fun setOnLoadMore(scrollParent: FrameLayout? = null, event: (totalItemCount: Int) -> Unit) {

        _onLoadMore = event

        if (scrollParent != null) {

            (scrollParent as View).viewTreeObserver.addOnScrollChangedListener {

                try {

                    val scrollDiff =
                        scrollParent.getChildAt(0)?.bottom?.minus(scrollParent.height + scrollParent.scrollY) ?: 0
                    if (scrollDiff == 0) {
                        _onLoadMore(0)
                    }

                } catch (e: Exception) {

                }

            }
            return
        }

        scrollListener.onLoadMore = _onLoadMore
    }

    fun setOnScroll(scrollParent: FrameLayout? = null, event: () -> Unit) {

        _onScroll = event

        if (scrollParent != null) {

            (scrollParent as View).viewTreeObserver.addOnScrollChangedListener {
                _onScroll()
            }

            return
        }

        scrollListener.onScroll = _onScroll
    }

    abstract class TAdapter<T : Any, VH : TViewHolder<T>>(var useDiffUtil: Boolean = false) : RecyclerView.Adapter<VH>() {

        private var _onItemClickListener: ((T) -> Unit)? = null

        private var mRefreshOperations = mutableListOf<List<T>>()
        private var mOperationPending = AtomicBoolean(false)

        private var mItems = mutableListOf<T>()

        private fun refreshWithDiffUtil() {

            if (mRefreshOperations.isNotEmpty() && mOperationPending.compareAndSet(false, true)) {
                updateItems(mRefreshOperations.removeAt(0))
            }
        }

        private fun refreshWithoutDiffUtil(items: List<T>) {

            mItems.clear()
            mItems.addAll(items)

            notifyDataSetChanged()
        }

        open fun refresh(items: List<T>) {

            if (useDiffUtil) {

                mRefreshOperations.add(items)

                refreshWithDiffUtil()
            } else {
                refreshWithoutDiffUtil(items)
            }
        }

        private fun updateItems(newItems: List<T>) {
            calculateDifference(newItems, mItems)
        }

        private fun calculateDifference(newItems: List<T>, oldItems: List<T>) {

            CoroutineScope(Dispatchers.Default).launch {

                withContext(Dispatchers.Default) {
                    DiffUtil.calculateDiff(
                        TDiffUtilCallback(
                            oldItems,
                            newItems
                        ), true
                    )
                }.also { diffResult ->

                    if (mRefreshOperations.isEmpty()) {

                        CoroutineScope(Dispatchers.Main).launch {

                            mItems.clear()
                            mItems.addAll(newItems)

                            diffResult.dispatchUpdatesTo(this@TAdapter)

                            mOperationPending.set(false)
                            refreshWithDiffUtil()
                        }
                    } else {
                        mOperationPending.set(false)
                        refreshWithDiffUtil()
                    }
                }

            }

        }

        fun getItemAt(position: Int): T? {

            return try {
                mItems[position]
            } catch (e: IndexOutOfBoundsException) {
                null
            }
        }

        override fun getItemCount(): Int {
            return mItems.size
        }

        fun setOnItemClickLister(event: (T) -> Unit) = apply { _onItemClickListener = event }

        override fun onBindViewHolder(holder: VH, position: Int) {
            if (holder.itemView.isClickable) {
                holder.itemView.setOnClickListener { getItemAt(position)?.let { _onItemClickListener?.invoke(it) } }
            }
        }

        override fun onBindViewHolder(holder: VH, position: Int, payloads: MutableList<Any>) {
            if (holder.itemView.isClickable) {
                holder.itemView.setOnClickListener { getItemAt(position)?.let { _onItemClickListener?.invoke(it) } }
            }

            super.onBindViewHolder(holder, position, payloads)
        }

    }

    abstract class TViewHolder<T : Any>(view: View) : RecyclerView.ViewHolder(view), BindableViewHolder<T> {

        companion object {

            inline fun <reified VH : ViewHolder> buildViewHolder(
                @LayoutRes res: Int, root: ViewGroup
            ): VH {

                val view = LayoutInflater.from(root.context).inflate(res, root, false)
                return VH::class.java.constructors[0].newInstance(view) as VH
            }
        }
    }

    class TDiffUtilCallback<T : Any>(private var mOldList: List<T>, private var mNewList: List<T>) : DiffUtil.Callback() {

        override fun getOldListSize(): Int {
            return mOldList.size
        }

        override fun getNewListSize(): Int {
            return mNewList.size
        }

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return mOldList[oldItemPosition].hashCode() == mNewList[newItemPosition].hashCode()
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return mOldList[oldItemPosition] == mNewList[newItemPosition]
        }
    }

    interface BindableViewHolder<T> {

        fun bind(item: T)
    }

}


