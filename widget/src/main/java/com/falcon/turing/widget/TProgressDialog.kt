@file:Suppress("DEPRECATION")

package com.falcon.turing.widget

import android.app.ProgressDialog
import android.content.Context
import com.falcon.turing.core.components.StringWrapper

class TProgressDialog(val context: Context?, var message: StringWrapper, var isCancelable: Boolean = true) {

    private var progressDialog: ProgressDialog? = null

    fun show() {

        progressDialog?.isShowing?.let { isShowing ->
            if (isShowing) {
                dismiss()
            }
        }

        progressDialog = context?.indeterminateProgressDialog(
            message = message(context)
        ).apply {
            this?.setCancelable(isCancelable)
        }
    }

    fun dismiss() {
        try {
            progressDialog?.dismiss()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun Context.indeterminateProgressDialog(message: String): ProgressDialog {
        return ProgressDialog(this).apply {
            isIndeterminate = true
            setMessage(message)
            show()
        }
    }

}