package com.falcon.turing.core.utils

import org.greenrobot.eventbus.EventBus

class EventBusUtil {

    companion object {

        fun register(subscriber: Any) {

            val bus = EventBus.getDefault()
            if (!bus.isRegistered(subscriber)) {
                bus.register(subscriber)
            }
        }

        fun unregister(subscriber: Any) {

            val bus = EventBus.getDefault()
            if (bus.isRegistered(subscriber)) {
                bus.unregister(subscriber)
            }
        }

    }

}