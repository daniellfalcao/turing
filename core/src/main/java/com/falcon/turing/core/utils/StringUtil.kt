package com.falcon.turing.core.utils

fun String.capitalizeWords(): String{
    var result = ""

    for(i in this.toLowerCase(BRASIL()).split(" ")){
        result += i.capitalize() + " "
    }

    return result
}