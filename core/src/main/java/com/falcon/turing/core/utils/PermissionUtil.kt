package com.falcon.turing.core.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment

/**
 * Constantes usadas para request code
 *
 * */
const val ACCESS_LOCATION_REQUEST_CODE = 700
const val ACCESS_CALENDAR_REQUEST_CODE = 701


/**
 * Constantes usadas para permissoes
 *
 * */
val ACCESS_LOCATION_PERMISSIONS = arrayOf(
    Manifest.permission.ACCESS_FINE_LOCATION,
    Manifest.permission.ACCESS_COARSE_LOCATION
)


val ACCESS_CALENDAR_PERMISSIONS = arrayOf(
    Manifest.permission.WRITE_CALENDAR,
    Manifest.permission.READ_CALENDAR
)

/**
 * Esse método retorna true se o usuário já tiver negado uma solicitação de permissão realizada pelo aplicativo.
 *
 * */
fun Activity.shouldShowRequestPermissionRationale(permissions: Array<String>): Boolean {

    for (permission in permissions) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
            return true
        }
    }

    return false
}

fun Array<String>.isPermissionsGranted(context: Context): Boolean {

    for (permission in this) {

        if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            return false
        }
    }

    return true
}

fun Context.isPermissionToAccessLocationGranted(): Boolean {
    return ACCESS_LOCATION_PERMISSIONS.isPermissionsGranted(this)
}

fun Activity.requestPermissionToAccessLocation(requestCode: Int = ACCESS_LOCATION_REQUEST_CODE) {
    ActivityCompat.requestPermissions(this, ACCESS_LOCATION_PERMISSIONS, ACCESS_LOCATION_REQUEST_CODE)
}

fun Fragment.requestPermissionToAccessLocation(requestCode: Int = ACCESS_LOCATION_REQUEST_CODE) {
    requestPermissions(ACCESS_LOCATION_PERMISSIONS, requestCode)
}

fun Context.isPermissionToAccessCalendarGranted(): Boolean {
    return ACCESS_CALENDAR_PERMISSIONS.isPermissionsGranted(this)
}

fun Activity.requestPermissionToAccessCalendar(requestCode: Int = ACCESS_CALENDAR_REQUEST_CODE) {
    ActivityCompat.requestPermissions(this, ACCESS_CALENDAR_PERMISSIONS, ACCESS_CALENDAR_REQUEST_CODE)
}

fun Fragment.requestPermissionToAccessCalendar(requestCode: Int = ACCESS_CALENDAR_REQUEST_CODE) {
    requestPermissions(ACCESS_CALENDAR_PERMISSIONS, ACCESS_CALENDAR_REQUEST_CODE)
}

