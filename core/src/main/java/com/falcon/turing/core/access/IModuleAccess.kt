package com.falcon.turing.core.access

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment

interface IModuleAccess<E: Enum<E>> {

    fun getModuleFragment(identifier: Enum<E>, bundle: Bundle? = null): Fragment?

    fun startModuleActivity(identifier: Enum<E>, context: Context, bundle: Bundle? = null, flag: Int? = null)
}