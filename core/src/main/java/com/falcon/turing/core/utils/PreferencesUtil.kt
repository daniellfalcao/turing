package com.falcon.turing.core.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.fragment.app.Fragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


/**
 * Recupera a instância do SharedPreferences para a preferenceKey passada, se não for passado nenhuma preference
 * key, é retornado a default shared preference
 *
 * @param preferenceKey chave
 *
 * @return a instância da shared preference
 */
fun Context.getPreferences(preferenceKey: String? = null): SharedPreferences {

    return preferenceKey?.let {
        this.getSharedPreferences(preferenceKey, Context.MODE_PRIVATE)
    } ?: kotlin.run {
        PreferenceManager.getDefaultSharedPreferences(this)
    }
}

fun Fragment.getPreferences(preferenceKey: String? = null) = activity?.getPreferences(preferenceKey)


/**
 * Lê a preferencia, retornando o valor padrão caso não exista
 *
 * @param context contexto
 * @param key chave da preferência
 * @param defaultValue valor padrão, retornado caso a preferência não exista
 *
 * @return o valor da preferência ou o defaultValue
 */
@Suppress("UNCHECKED_CAST", "IMPLICIT_CAST_TO_ANY")
fun <T> SharedPreferences.read(key: String, defaultValue: T): T {

    return when (defaultValue) {
        is Boolean -> getBoolean(key, defaultValue)
        is Int -> getInt(key, defaultValue)
        is Long -> getLong(key, defaultValue)
        is Float -> getFloat(key, defaultValue)
        is String -> getString(key, defaultValue)
        else -> throw IllegalArgumentException()
    } as T
}

/**
 * Escreve a preferência
 *
 * @param context contexto
 * @param key chave da preferência
 * @param value valor a ser escrito na preferência
 */
@SuppressLint("ApplySharedPref")
fun SharedPreferences.write(param: Pair<String, Any>, onNext: () -> Unit = {}) {

    val editor = edit()

    val key = param.first
    val value = param.second

    when (value) {
        is Boolean -> editor.putBoolean(key, value)
        is Int -> editor.putInt(key, value)
        is Long -> editor.putLong(key, value)
        is Float -> editor.putFloat(key, value)
        is String -> editor.putString(key, value)
        else -> throw IllegalArgumentException()
    }

    CoroutineScope(Dispatchers.IO).launch {
        editor.commit()
        CoroutineScope(Dispatchers.Main).launch { onNext() }
    }

}


/**
 * Apaga uma preference
 * */
@SuppressLint("ApplySharedPref")
fun SharedPreferences.clearFromKey(key: String, onNext: () -> Unit = {}) {

    CoroutineScope(Dispatchers.Default).launch {
        edit().remove(key).commit()
        CoroutineScope(Dispatchers.Main).launch { onNext() }
    }
}

@SuppressLint("ApplySharedPref")
fun SharedPreferences.clearFromKeys(vararg keys: String, onNext: () -> Unit = {}) {
    val editor = edit()

    keys.forEach {
        editor.remove(it)
    }

    CoroutineScope(Dispatchers.Default).launch {
        editor.commit()
        CoroutineScope(Dispatchers.Main).launch { onNext() }
    }
}