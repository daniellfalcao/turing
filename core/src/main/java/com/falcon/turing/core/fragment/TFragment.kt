package com.falcon.turing.core.fragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.falcon.turing.core.activity.TActivity
import com.falcon.turing.core.event.BaseEvent
import com.falcon.turing.core.utils.EventBusUtil
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

abstract class TFragment() : Fragment() {

    protected val BUS: EventBus by lazy { EventBus.getDefault() }
    open var eventBusEnabled: Boolean = true

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (eventBusEnabled) EventBusUtil.register(this)
    }

    override fun onResume() {
        super.onResume()
        if (eventBusEnabled) EventBusUtil.register(this)
    }

    override fun onPause() {
        super.onPause()
        if (eventBusEnabled) EventBusUtil.unregister(this)
    }

    @Suppress("UNUSED_PARAMETER")
    @Subscribe
    fun onEvent(event: BaseEvent) {

    }

}

@Suppress("UNCHECKED_CAST")
fun <T : TActivity> TFragment.withActivity(event: T.() -> Unit) {
    (activity as? T)?.run(event)
}

fun TFragment.withParentFragment(event: Fragment.() -> Unit) {
    parentFragment?.run(event)
}

fun TFragment.withContext(event: Context.() -> Unit) {
    context?.run(event)
}

fun TFragment.withArguments(event: Bundle.() -> Unit) {
    this.arguments?.run(event)
}