package com.falcon.turing.core.utils

import androidx.recyclerview.widget.RecyclerView
import com.rohit.recycleritemclicksupport.RecyclerItemClickSupport

fun RecyclerView.setOnItemClickListener(onClick: (recyclerView: RecyclerView, position: Int, viewClicked: RecyclerView.ViewHolder?) -> Unit) {
    RecyclerItemClickSupport.addTo(this).setOnItemClickListener { recyclerView, position, _ ->
        recyclerView?.adapter?.let { adapter ->
            if (position != RecyclerView.NO_POSITION && position < adapter.itemCount) {
                onClick(this, position, recyclerView.findViewHolderForAdapterPosition(position))
            }
        }
    }
}

fun RecyclerView.setOnItemLongClickListener(onLongClick: (recyclerView: RecyclerView, position: Int, viewClicked: RecyclerView.ViewHolder?) -> Unit) {
    RecyclerItemClickSupport.addTo(this).setOnItemLongClickListener { recyclerView, position, _ ->
        recyclerView?.adapter?.let { adapter ->
            if (position != RecyclerView.NO_POSITION && position < adapter.itemCount) {
                onLongClick(this, position, recyclerView.findViewHolderForAdapterPosition(position))
            }
        }
        true
    }
}