package com.falcon.turing.core.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.falcon.turing.core.event.BaseEvent
import com.falcon.turing.core.utils.EventBusUtil
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

abstract class TActivity : AppCompatActivity() {

    open val BUS: EventBus by lazy { EventBus.getDefault() }
    open var eventBusEnabled: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (eventBusEnabled) EventBusUtil.register(this)
    }

    override fun onResume() {
        super.onResume()
        if (eventBusEnabled) EventBusUtil.register(this)
    }

    override fun onPause() {
        super.onPause()
        if (eventBusEnabled) EventBusUtil.unregister(this)
    }

    @Suppress("UNUSED_PARAMETER")
    @Subscribe
    fun onEvent(event: BaseEvent) {
    }
}

fun TActivity.withIntent(event: Intent.() -> Unit) {
    this.intent?.run(event)
}