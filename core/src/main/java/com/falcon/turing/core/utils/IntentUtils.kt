package com.falcon.turing.core.utils

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Browser
import androidx.fragment.app.Fragment

const val CHROME_PACKAGE = "com.android.chrome"

/**
 * Redireciona a url para o chrome.
 *
 * @param url url a ser redirecionada;
 * @param headers headers da intent;
 * @param onActivityNotFound função de callback;
 * */
fun Context.openBrowser(
    url: String,
    vararg headers: Pair<String, String>?,
    onActivityNotFound: (() -> Unit) = {}
) {

    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))

    headers.let {

        val bundle = Bundle()

        intent.setPackage(CHROME_PACKAGE)

        it.forEach { header ->
            bundle.putString(header?.first, header?.second)
        }

        intent.putExtra(Browser.EXTRA_HEADERS, bundle)
    }

    try {
        startActivity(intent)
    } catch (e: ActivityNotFoundException) {
        onActivityNotFound()
    }

}

/**
 * @see Context.openBrowser
 * */
fun Fragment.openBrowser(
    url: String,
    vararg headers: Pair<String, String>?,
    onActivityNotFound: (() -> Unit) = {}
) = activity?.openBrowser(url = url, headers = *headers, onActivityNotFound = onActivityNotFound)
