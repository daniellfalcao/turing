package com.falcon.turing.lifecycle.util

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

fun <T> MutableLiveData<T>.notifyDataChanged(value: T) {
    this.value = value
}