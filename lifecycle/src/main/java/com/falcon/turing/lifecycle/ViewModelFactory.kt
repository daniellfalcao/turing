package com.falcon.turing.lifecycle

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders

class ViewModelFactory(private val viewModel: ViewModel) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return viewModel as T
    }
}

inline fun <reified T : ViewModel> Fragment.getViewModel(noinline creator: (() -> T)? = null): T {

    return creator?.let {
        ViewModelProviders.of(this, ViewModelFactory(creator.invoke())).get(T::class.java)
    } ?: kotlin.run {
        ViewModelProviders.of(this).get(T::class.java)
    }
}

inline fun <reified T : ViewModel> FragmentActivity.getViewModel(noinline creator: (() -> T)? = null): T {

    return creator?.let {
        ViewModelProviders.of(this, ViewModelFactory(creator.invoke())).get(T::class.java)
    } ?: kotlin.run {
        ViewModelProviders.of(this).get(T::class.java)
    }
}