package com.falcon.turing.lifecycle.util

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Usado para empacotar um dado que pode ser exposto via LiveData representando um evento.
 */
abstract class Event<T> {

    protected open var hasBeenHandled = AtomicBoolean(false)
    protected open var content: T? = null

    /**
     * Retorna o conteúdo do evento e previne que ele sejá usado novamente
     */
    open fun getContentIfNotHandled(): T? {
        return if (hasBeenHandled.compareAndSet(false, true)) {
            content
        } else {
            null
        }
    }

    /**
     * Retorna o conteúdo do evento mesmo que ele já tenha sido usado.
     */
    open fun peekContent(): T? = content

    class Data<T>(override var content: T?) : Event<T>()

    class Callback : Event<Any>() {
        override var content: Any? = "CALLBACK"
    }
}


/**
 * Registra um observer em eventos de Data, caso não exista dados (null) o evento não será chamado.
 * */
@JvmName("observeDataEvent")
fun <T> LiveData<Event.Data<T>>.observeEvent(owner: LifecycleOwner, observer: (T) -> Unit) {
    observe(owner, Observer { event -> event.getContentIfNotHandled()?.let { observer(it) } })
}

/**
 * Registra um observer em eventos de Callback
 * */
@JvmName("observeCallbackEvent")
fun LiveData<Event.Callback>.observeEvent(owner: LifecycleOwner, observer: () -> Unit) {
    observe(owner, Observer { event -> event.getContentIfNotHandled()?.let { observer() } })
}

fun MutableLiveData<Event.Callback>.call() {
    this.value = Event.Callback()
}
