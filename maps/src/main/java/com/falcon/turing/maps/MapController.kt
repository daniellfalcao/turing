package com.falcon.turing.maps

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.falcon.turing.core.utils.isPermissionToAccessLocationGranted
import com.falcon.turing.location.LocationController
import com.falcon.turing.maps.MapController.MapStyle.SATELLITE
import com.falcon.turing.maps.MapController.MapStyle.STANDART
import com.falcon.turing.maps.MapController.MarkersState.*
import com.falcon.turing.maps.data.IMarker
import com.falcon.turing.maps.data.IPerimeter
import com.falcon.turing.maps.utils.onlyMarkers
import com.falcon.turing.maps.utils.onlyPerimeters
import com.falcon.turing.maps.utils.toLatLng
import com.falcon.turing.maps.utils.toMarkerOptions
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL
import com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.maps.android.PolyUtil
import kotlinx.coroutines.*

class MapController(val context: Context) {

    enum class MapStyle { STANDART , SATELLITE }

    // dispatcher de execução na ui
    private val UIDispatcher: CoroutineDispatcher = Dispatchers.Main
    private val UIScope = CoroutineScope(UIDispatcher)

    // dispatcher de execução em background
    val BGDispatcher: CoroutineDispatcher = Dispatchers.IO
    val BGScope = CoroutineScope(BGDispatcher)

    /**
     *
     * Parâmetros de configuracão do mapa
     *
     * */

    // variável de controle de exibição da localização do usuário.
    // por default se deve mostrar a localização do usuário no mapa.
    private var _shouldShowUserLocation: (lastUserLocation: LatLng?) -> Boolean = { true }

    fun shouldShowUserLocation(event: (lastUserLocation: LatLng?) -> Boolean) =
        apply { _shouldShowUserLocation = event }

    // variável de controle de atualização da view do mapa de acordo com a bussola do celular.
    val updateMapWithBearing = false

    var style: MapStyle = STANDART
    set(value) {

        field = value

        when (value) {

            STANDART -> googleMap?.mapType = MAP_TYPE_NORMAL
            SATELLITE -> googleMap?.mapType = MAP_TYPE_SATELLITE
        }

        googleMap?.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.standart_map_style))
    }


    /**
     *
     * Controllers
     *
     * */

    val locationController = LocationController()
    val navigationController = NavigationController()
    val markerController = MarkerController()
    val perimeterController = PerimeterController()

    // mapa
    var googleMap: GoogleMap? = null

    /**
     *
     * Callbacks
     *
     * */

    private val onLocationChanged = locationController.addOnLocationChanged { lastLocation ->
        // do something
    }

    // TODO: quando tiver mapa checar se o resto ta configurado
    fun setupMap(mapFragment: Fragment, onMapReady: MapController.(googleMap: GoogleMap) -> Unit = { }) {

        (mapFragment as? SupportMapFragment)?.getMapAsync {

            googleMap = it

            style = STANDART

            locationController.startLocationService(context)

            onMapReady(it)

            enableUserLocation()
        }

    }

    fun onPermissionsChanged() {

        enableUserLocation()
        locationController.startLocationService(context)
    }

    @SuppressLint("MissingPermission")
    private fun enableUserLocation() {

        if (context.isPermissionToAccessLocationGranted()) {
            googleMap?.isMyLocationEnabled = _shouldShowUserLocation(locationController.lastLocation?.toLatLng())
        }
    }

    inner class NavigationController {

        val TAG = "NavigationController"

        /**
         *
         * Parametros de controle e configuracao da navegacao
         *
         * */

        // indica se existe alguma navegacao em andamento
        var isNavigationInProgress = false
            private set(value) {

                field = value

                if (isNavigationInProgress) {
                    destination?.let { _onNavigationStarted.forEach { event -> event(it) } }
                } else {
                    _onNavigationFinished.forEach { it() }
                }

                markerController.refresh()

            }

        // variável de controle de exibição da rota de navegação do usuário.
        // por default se deve mostrar sempre a a rota traçada pela navegacao a um destino/
        private var _shouldShowNavigationRoute: (lastUserLocation: LatLng?) -> Boolean = { true }

        fun shouldShowNavigationRoute(event: (lastUserLocation: LatLng?) -> Boolean) =
            apply { _shouldShowNavigationRoute = event }

        // variável de controle para encerrar a navegação do usuário.
        // por default a navegação nunca sera encerrada a não ser que ela sejá configurada.
        private var _shouldFinishNavigation: (lastUserLocation: LatLng, destinationLocation: LatLng) -> Boolean =
            { _, _ -> false }

        fun shouldFinishNavigation(event: (lastUserLocation: LatLng, destinationLocation: LatLng) -> Boolean) = apply {
            _shouldFinishNavigation = event
        }

        /**
         *
         * View e configuracoes da rota
         *
         * */

        // linha que é exibida quando existe alguma rota tracada
        private var route: Polyline? = null

        // configuracao da polyline que sera exibida durante a a navegacao
        private var routeConfiguration: PolylineOptions? = null

        // marker de destino
        private var destinationMarker: Marker? = null

        // path da navegacao, a ultima posicao deve ser o destino
        var path: MutableList<IMarker> = mutableListOf()
            set(value) {

                field = value

                if (field.isNotEmpty()) {

                    destination = field[field.size.minus(1)]

                    if (isNavigationInProgress) {
                        updateRoute()
                    }
                }
            }

        // destino da navegacao
        var destination: IMarker? = null
            private set(value) {
                field = value
            }

        /**
         *
         * Callbacks
         *
         * */

        // callback de quando a navegação for encerrada
        private var _onNavigationFinished: MutableList<() -> Unit> = mutableListOf()

        fun addOnNavigationFinished(event: () -> Unit) = apply { _onNavigationFinished.add(event) }

        // callback de quando a navegação for iniciada
        private var _onNavigationStarted: MutableList<(destination: IMarker) -> Unit> = mutableListOf()

        fun addOnNavigationStarted(event: (destination: IMarker) -> Unit) = apply { _onNavigationStarted.add(event) }

        init {

            locationController.addOnLocationChanged {

                if (isNavigationInProgress && path.isNotEmpty()) {
                    updateRoute()
                    if (_shouldFinishNavigation(it.toLatLng(), path[path.size.minus(1)].getLocalePosition())) {
                        endNavigation()
                    }
                }
            }
        }

        fun startNavigation() {

            if (path.isNotEmpty()) {

                isNavigationInProgress = true

                updateRoute()

            } else {
                Log.d(TAG, "Nao existe path para que a navegação seja iniciada")
            }
        }

        fun endNavigation() {

            // remove a rota
            route?.remove()

            // remove o marcador de destino
            destinationMarker?.remove()
            destinationMarker = null

            path = mutableListOf()
            destination = null

            isNavigationInProgress = false

            markerController.refresh()
        }

        fun updateRoute() {

            destination?.let { drawDestinationMarker(it) }
            drawRoute()
        }

        private fun drawDestinationMarker(destination: IMarker) {

            if ((destinationMarker != null && destinationMarker!!.position != destination.getLocalePosition())
                || destinationMarker == null
            ) {

                destinationMarker?.remove()
                destinationMarker = googleMap?.addMarker(
                    destination.toMarkerOptions(
                        context,
                        destination.getMarkerInNavigationIcon(),
                        true
                    )
                )
            }
        }

        private fun drawRoute() {

            routeConfiguration = PolylineOptions().apply {

                locationController.lastLocation?.toLatLng()?.let {
                    add(it)
                }

                path.forEach { add(it.getLocalePosition()) }

                width(5F)
                color(ContextCompat.getColor(context, R.color.turing_maps_default_route_color))
                visible(_shouldShowNavigationRoute(locationController.lastLocation?.toLatLng()))
            }

            route?.remove()

            route = googleMap?.addPolyline(routeConfiguration)
        }
    }

    enum class MarkersState { SHOW, HIDE }

    inner class MarkerController {

        // marcadores que estão visiveis no mapa atualmente de acordo com os seus tipos
        private var mapMarkers: HashMap<String, MutableList<Marker>> = hashMapOf() // ???
        private var _iMarkers: MutableList<IMarker> = mutableListOf()

        var markersState: MarkersState = SHOW

        private var refreshJob: Job? = null

        // variavel de controle da visibilidade dos marcadores
        private var _shouldShowMarker: (marker: IMarker) -> Boolean = { true }

        private var _onMarkersStateChanged: MutableList<(state: MarkersState) -> Unit> = mutableListOf()
        fun addOnMarkersStateChanged(event: (state: MarkersState) -> Unit) = apply { _onMarkersStateChanged.add(event) }

        fun shouldShowMarker(event: (marker: IMarker) -> Boolean) = apply { _shouldShowMarker = event }

        /**
         *
         * Callback
         *
         * */

        private var _onMarkersChanged: MutableList<() -> Unit> = mutableListOf()

        fun addOnMarkersRefreshed(event: () -> Unit) = apply { _onMarkersChanged.add(event) }

        fun refresh() {

            googleMap?.let {
                refreshMarkers(_iMarkers)
            }
        }

        // TODO: melhorar draw de marcadores (dimnuir a quanidade de views que piscam)
        fun refreshMarkers(markers: List<IMarker>) {

            _iMarkers = markers.onlyMarkers().toMutableList()

            refreshJob?.cancel()

            refreshJob = UIScope.launch {

                // remove todos os marcadores que estiverem no mapa
                mapMarkers.forEach { map -> map.value.forEach { it.remove() } }

                val markersView: HashMap<String, MutableList<Marker>> = hashMapOf()

                _iMarkers.forEach { marker ->

                    // se não existir o tipo do marcador dentro dos markerOptions ele deve ser criado
                    if (markersView[marker.getMarkerType()] == null) {
                        markersView[marker.getMarkerType()] = mutableListOf()
                    }

                    googleMap?.addMarker(marker.toMarkerOptions(context, marker.getMarkerIcon(), _shouldShowMarker(marker)))?.let {
                        markersView[marker.getMarkerType()]?.add(it)
                    }

                }

                mapMarkers = markersView

                _onMarkersChanged.forEach { it() }
            }.apply { start() }
        }

        fun showAllMarkers(vararg unless: String) {

            mapMarkers.forEach { markers ->

                if (!unless.contains(markers.key)) {
                    markers.value.forEach {
                        it.isVisible = true
                    }
                }
            }

            markersState = SHOW
            _onMarkersStateChanged.forEach { it(markersState) }
        }

        fun hideAllMarkers(vararg unless: String) {

            mapMarkers.forEach { markers ->

                if (!unless.contains(markers.key)) {
                    markers.value.forEach {
                        it.isVisible = false
                    }
                }
            }

            markersState = HIDE
            _onMarkersStateChanged.forEach { it(markersState) }
        }

        fun hideMarker(marker: IMarker) {

            mapMarkers.forEach { map ->

                map.value.forEach { markerView ->

                    markerView.takeIf { it.position == marker.getLocalePosition() }?.let {
                        it.isVisible = false
                        return
                    }
                }
            }

        }

    }

    inner class PerimeterController {

        // perimetros que serao demarcados no mapa
        private var mapPerimeter: HashMap<String, Polygon> = hashMapOf()
        private var _iPerimeters: MutableList<IPerimeter> = mutableListOf()

        private var refreshJob: Job? = null

        private var _onPerimeterChanged: MutableList<() -> Unit> = mutableListOf()
        fun addOnPerimeterRefreshed(event: () -> Unit) = apply { _onPerimeterChanged.add(event) }

        fun refresh() {

            googleMap?.let {
                refreshPerimeter(_iPerimeters)
            }
        }

        fun refreshPerimeter(perimeter: List<IPerimeter>) {

            _iPerimeters = perimeter.onlyPerimeters().toMutableList()

            refreshJob?.cancel()

            refreshJob = UIScope.launch {

                // remove os perimetros que estiverem no mapa
                mapPerimeter.forEach { it.value.remove() }

                val perimeterView: HashMap<String, Polygon> = hashMapOf()

                _iPerimeters.groupBy { it.getPerimeterType() }.forEach { map ->

                    val polygonOptions = PolygonOptions().apply {

                        map.value.forEach { perimeter ->
                            add(perimeter.getLocalePosition())
                        }

                        strokeWidth(3F)
                        strokeColor(ContextCompat.getColor(context, R.color.turing_maps_perimeter_stroke_color))

                        fillColor(ContextCompat.getColor(context, R.color.turing_maps_perimeter_fill_color))
                    }

                    googleMap?.let {
                        perimeterView[map.key] = it.addPolygon(polygonOptions)
                    }
                }

                mapPerimeter = perimeterView

                _onPerimeterChanged.forEach { it() }
            }.apply { start() }
        }

        fun isUserInsidePerimeter(): Boolean {

            return locationController.lastLocation?.let { location ->

                var containsLocation = false

                mapPerimeter.forEach { map ->

                    containsLocation = PolyUtil.containsLocation(location.toLatLng(), map.value.points, false)

                    if (containsLocation) {
                        return@forEach
                    }
                }

                containsLocation
            } ?: kotlin.run { false }

        }

        fun existsPerimeter(): Boolean {
            return mapPerimeter.isNotEmpty()
        }
    }

}