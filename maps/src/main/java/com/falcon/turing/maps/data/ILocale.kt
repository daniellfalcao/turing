package com.falcon.turing.maps.data

import com.google.android.gms.maps.model.LatLng

interface ILocale {

    fun getLocaleID(): String
    fun getLocalePosition(): LatLng
}