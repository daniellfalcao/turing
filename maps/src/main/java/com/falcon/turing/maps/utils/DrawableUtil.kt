package com.falcon.turing.maps.utils

import android.content.Context
import android.graphics.Bitmap.Config
import android.graphics.Bitmap.createBitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory

fun Drawable.toBitmapDescriptor(context: Context): BitmapDescriptor {

    this.setBounds(0, 0, intrinsicWidth, intrinsicHeight)

    val bitmap = createBitmap(intrinsicWidth, intrinsicHeight, Config.ARGB_8888)

    Canvas(bitmap).apply { draw(this) }

    return BitmapDescriptorFactory.fromBitmap(bitmap)
}