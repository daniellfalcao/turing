package com.falcon.turing.maps.data

interface IMarker : ILocale {

    fun getMarkerType(): String
    fun getMarkerTitle(): String
    fun getMarkerIcon(): Int
    fun getMarkerInNavigationIcon(): Int
    fun isMarker(): Boolean
}