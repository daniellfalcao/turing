package com.falcon.turing.maps.utils

import android.content.Context
import androidx.annotation.DrawableRes
import com.falcon.turing.maps.data.IMarker
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions


fun IMarker.toMarkerOptions(context: Context, @DrawableRes icon: Int, isVisible: Boolean): MarkerOptions {

    return MarkerOptions().apply {

        position(getLocalePosition())
        title(getMarkerTitle())
        visible(isVisible)
        icon(context.getDrawable(icon)?.toBitmapDescriptor(context))
    }
}

fun List<IMarker>.getMarkerAtPosition(locale: LatLng): IMarker? {

    return this.firstOrNull{ it.getLocalePosition() == locale }
}