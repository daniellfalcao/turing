package com.falcon.turing.maps.utils

import android.location.Location
import com.falcon.turing.maps.data.IMarker
import com.falcon.turing.maps.data.IPerimeter
import com.falcon.turing.maps.utils.MergeConflict.*
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.SphericalUtil

enum class MergeConflict { REPLACE, KEEP }

fun LatLng.distanceTo(locale: LatLng): Double {
    return SphericalUtil.computeDistanceBetween(this, locale)
}

fun Location.toLatLng(): LatLng {
    return LatLng(latitude, longitude)
}

fun List<IPerimeter>.onlyPerimeters(): List<IPerimeter> {

    val filteredPerimeter: MutableList<IPerimeter> = mutableListOf()

    this.groupBy { it.isPerimeter() }.apply {

        get(true)?.groupBy { it.getPerimeterType() }.apply {
            this?.forEach { map ->
                filteredPerimeter.addAll(map.value)
            }
        }
    }

    return filteredPerimeter
}

fun List<IMarker>.onlyMarkers(): List<IMarker> {

    val filteredMarkers: MutableList<IMarker> = mutableListOf()

    // faz o agrupamento de quem é marcardo e de quem não é
    this.groupBy { it.isMarker() }.apply {

        // apos separa o que é marcador do que não é, pega todos os marcadores e separa eles por tipo
        get(true)?.groupBy { it.getMarkerType() }.apply {

            // pega todos os tipos de marcadores que foram encontrados e add na lista de marcadores
            this?.forEach { groupMarkers ->
                filteredMarkers.addAll(groupMarkers.value)
            }
        }
    }

    return filteredMarkers

}

suspend fun List<IMarker>.merge(markers: List<IMarker>, onConflict: MergeConflict = REPLACE): List<IMarker> {

    val mergedMarkers: MutableList<IMarker> = mutableListOf()
    val currentMarkers: MutableList<IMarker> = this.toMutableList()

    markers.forEach { marker ->

        currentMarkers.firstOrNull { it.getLocaleID() == marker.getLocaleID() }?.let {

            when(onConflict) {
                REPLACE -> mergedMarkers.add(marker)
                KEEP    -> mergedMarkers.add(it)
            }

            currentMarkers.remove(it)

        } ?: kotlin.run {
            mergedMarkers.add(marker)
        }
    }

    // adiciona todos os marcadores que sobraram
    mergedMarkers.addAll(currentMarkers)

    return mergedMarkers
}