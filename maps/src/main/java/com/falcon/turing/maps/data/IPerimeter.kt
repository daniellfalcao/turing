package com.falcon.turing.maps.data

interface IPerimeter : ILocale {

    fun getPerimeterType(): String
    fun isPerimeter(): Boolean
}