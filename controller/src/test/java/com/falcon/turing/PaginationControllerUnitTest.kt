package com.falcon.turing

import com.falcon.turing.controller.PaginationController
import org.junit.Test

class PaginationControllerUnitTest {

    var paginaAtual = 0
    private var paginaCorreta = false
    private var tamanhoDaPaginaCorreto = false
    private var ultimaPaginaCancelada  = false

    private fun resetaParametrosDeTeste() {
        paginaAtual = 0
        paginaCorreta = false
        tamanhoDaPaginaCorreto = false
        ultimaPaginaCancelada = false
    }

    @Test
    fun `testa a paginacao com 2 paginas`() {

        val controller = PaginationController(20,1)
        resetaParametrosDeTeste()

        controller.setOnCancelCurrentPage { page, origin ->
            throw Exception()
        }

        controller.setOnCancelCurrentPage { page ->
            ultimaPaginaCancelada = paginaAtual == page
        }

        controller.setOnPageRequested { page, pageSize, origin ->
            throw Exception("setOnPageRequested com origin nao deve ser chamado")
        }

        controller.setOnPageRequested { page, pageSize ->
            paginaCorreta = page == paginaAtual
            tamanhoDaPaginaCorreto = pageSize == 20
        }

        paginaAtual = 1
        controller.getNextPage(true)

        controller.notifyPaggingSuccessful(20)
        controller.notifityPaggingFinished()

        resetaParametrosDeTeste()
        paginaAtual = 2

        controller.getNextPage()

        assert(paginaCorreta)
        assert(tamanhoDaPaginaCorreto)
        assert(ultimaPaginaCancelada)
    }

    @Test
    fun `testa a paginacao com 1 pagina e finaliza`() {

        val controller = PaginationController(20,1)
        resetaParametrosDeTeste()

        controller.setOnCancelCurrentPage { page, origin ->
            throw Exception()
        }

        controller.setOnCancelCurrentPage { page ->
            ultimaPaginaCancelada = paginaAtual == page
        }

        controller.setOnPageRequested { page, pageSize, origin ->
            throw Exception("setOnPageRequested com origin nao deve ser chamado")
        }

        controller.setOnPageRequested { page, pageSize ->
            paginaCorreta = page == paginaAtual
            tamanhoDaPaginaCorreto = pageSize == 20
        }

        paginaAtual = 1
        controller.getNextPage(true)

        controller.notifyPaggingSuccessful(18)
        controller.notifityPaggingFinished()

        resetaParametrosDeTeste()
        paginaAtual = 2

        controller.getNextPage()

        assert(controller.getPageManager()!!.currentPage == 1)
        assert(!tamanhoDaPaginaCorreto)
        assert(!ultimaPaginaCancelada)

    }
}
