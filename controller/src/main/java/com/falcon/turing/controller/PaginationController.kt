package com.falcon.turing.controller

import java.util.concurrent.atomic.AtomicBoolean

open class PaginationController(private val pageLimit: Int, private val firstPage: Int = 1) {

    companion object {

        // origin de paginacao default, deve ser usado quando nao ha mais de um por tela
        const val DEFAULT_MANAGER = "DEFAULT_MANAGER"
    }

    sealed class PaginationState {

        // estado de repouso da paginacao
        object Idle : PaginationState()

        // estado de paginacao em andamento
        class Pagging(val page: Int, val isFirstPage: Boolean) : PaginationState()
    }

    class PageManager(val limit: Int, val firstPage: Int = 1) {

        var currentPage = firstPage.minus(1)
        var isLastPage = false

        var hasPendingPaginations = AtomicBoolean(false)

        /**
         * Verifica se o manager permite pegar uma proxima pagina. O manager pode recusar
         * pegar a proxima pagina de 2 formas:
         *
         * <1> Caso ja tenha chegado ao final de todas as paginas;
         * <2> Caso ja exista alguma paginacao pendente de finalizacao;
         *
         * @return true se for permitido pegar a proxima pagina;
         * @return false se não for permitido pegar a proxima pagina;
         * */
        fun allowGetNextPage(): Boolean {

            // verifica se já chegou a ultima pagina
            if (isLastPage) return false

            // verifica se ainda existe alguma paginacao pendente de finalizacao
            if (hasPendingPaginations.get()) return false

            return true
        }

        /**
         * @return o numero da proxima pagina.
         * */
        fun getNextPage() = currentPage.plus(1)

        /**
         * Reseta o manager para o estado inicial.
         * */
        fun reset() {
            currentPage = firstPage.minus(1)
            isLastPage = false
            hasPendingPaginations = AtomicBoolean(false)
        }
    }

    var state: PaginationState = PaginationState.Idle

    // hash map contendo todos os managers de diferentes origens
    private val hashPageManager: HashMap<String, PageManager> = HashMap(1)

    // callback de cancelamento de request
    protected var _onCancelCurrentPage: (page: Int) -> Unit = {_ -> }
    protected var _onCancelCurrentPageForOrigem: (page: Int, origin: String) -> Unit = {_, _ -> }

    // callback de chamada de request de paginacao
    protected var _onPageRequested: (page: Int, pageSize: Int) -> Unit = { _, _ -> }
    protected var _onPageRequestedForOrigem: (page: Int, pageSize: Int, origin: String) -> Unit = { _, _, _ -> }

    // callback de mudanca de status de paginacao
    protected var _onPageStateChanged: (state: PaginationState, origin: String) -> Unit = { _, _ -> }

    init {
        hashPageManager[DEFAULT_MANAGER] = PageManager(pageLimit, firstPage)
    }

    /**
     * Seta o callback de cancelamento de paginacao.
     *
     * @param event callback de cancelamento de paginacao.
     * */
    fun setOnCancelCurrentPage(event: (page: Int) -> Unit) = apply { _onCancelCurrentPage = event }

    /**
     * Seta o callback de cancelamento de paginacao para uma determinada origem de dados.
     *
     * @param event callback de cancelamento de paginacao de uma determinada origem.
     * */
    fun setOnCancelCurrentPage(event: (page: Int, origin: String) -> Unit) = apply { _onCancelCurrentPageForOrigem = event }

    /**
     * Seta o callback de solicitacao de nova pagina.
     *
     * @param event callback de solicitacao de nova pagina.
     * */
    fun setOnPageRequested(event: (page: Int, pageSize: Int) -> Unit) = apply { _onPageRequested = event }

    /**
     * Seta o callback de solicitacao de nova pagina para uma determinada origem de dados.
     *
     * @param event callback de solicitacao de nova pagina com origem.
     * */
    fun setOnPageRequested(event: (page: Int, pageSize: Int, origin: String) -> Unit) = apply { _onPageRequestedForOrigem = event }

    /**
     * Seta o callback de mudanca de status de paginacao
     *
     * @param event callback de mudanca de status
     * */
    fun setOnPaginationStateChanged(event: (state: PaginationState, origin: String) -> Unit) = apply { _onPageStateChanged = event }

    /**
     * Tenta pegar a proxima pagina validando se o manager permite ou nao pegar a proxima pagina de acordo com a origin.
     *
     * @see PageManager.allowGetNextPage
     *
     * @param reset informa se a paginacao deve ser reiniciada
     * @param origin informa a origem dos dados da paginacao. EX: busca paginada, filtro paginado.
     * */
    fun getNextPage(reset: Boolean = false, origin: String = DEFAULT_MANAGER) = synchronized(this) {

        // inicializa o pageManager caso ele ainda nao tenha sido criado
        val pageManager = hashPageManager[origin] ?: run {
            hashPageManager[origin] = PageManager(pageLimit, firstPage)
            hashPageManager[origin]!!
        }

        // verifica se no momento esta sendo permitido ser feito algum tipo de paginacao
        if (pageManager.allowGetNextPage() || reset) {

            // se a paginacao for ser feita por reset, restaura o pageManager para o estado inicial
            if (reset) pageManager.reset()

            // avisa para o pageManager que existe uma paginacao pendente
            pageManager.hasPendingPaginations.set(true)

            // antes de iniciar o processo de paginacao e chamado um callback para informar que a
            // ultima paginacao deve ser cancelada se for necessario.
            if (origin == DEFAULT_MANAGER) {
                _onCancelCurrentPage(pageManager.getNextPage())
            } else {
                _onCancelCurrentPageForOrigem(pageManager.getNextPage(), origin)
            }

            // inicia o processo de paginacao, chamando o callback de requestPage
            if (origin == DEFAULT_MANAGER) {
                _onPageRequested(pageManager.getNextPage(), pageManager.limit)
            } else {
                _onPageRequestedForOrigem(pageManager.getNextPage(), pageManager.limit, origin)
            }
        }
    }

    /**
     * Notifica para a controller que a paginacao foi iniciada.
     * Essa funcao deve ser sempre chamada assim que a paginacao for iniciada.
     * */
    fun notifyPaggingStarted(page: Int, origin: String = DEFAULT_MANAGER) {
        state = PaginationState.Pagging(page, page == firstPage)
        _onPageStateChanged(state, origin)
    }

    /**
     * Notifica para a controller que a paginacao foi finalizada.
     * Essa funcao deve ser sempre chamada asim que a paginacao for finalizada, independente de status.
     * */
    fun notifityPaggingFinished(origin: String = DEFAULT_MANAGER) {

        val pageManager = hashPageManager[origin]!!

        pageManager.hasPendingPaginations.set(false)
        state = PaginationState.Idle
        _onPageStateChanged(PaginationState.Idle, origin)
    }

    /**
     * Notifica para a controller que a paginacao da origin foi finalizada com sucesso.
     * Essa funcao deve ser sempre chamada assim que a paginacao for finalizada com sucesso.
     * */
    fun notifyPaggingSuccessful(pageSize: Int, origin: String = DEFAULT_MANAGER) {

        val pageManager = hashPageManager[origin]!!

        // no sucesso da requisicao de paginacao atualiza os valores
        pageManager.currentPage += 1

        if (pageSize < pageLimit) pageManager.isLastPage = true
    }

    /**
     * Verifica se o numero da pagina é a primeira a ser solicitada
     *
     * @param page pagina a ser verificada
     *
     * @return true se page for a primeira que e solicitada
     * @return false se a page nao for a primeira que e solicitada
     * */

    fun isFirstPage(page: Int): Boolean {
        return page == firstPage
    }

    /**
     * Pega o manager de uma determinada origem.
     *
     * @param origin a origem do manager
     * */
    fun getPageManager(origin: String = DEFAULT_MANAGER): PageManager? {
        return hashPageManager[origin]
    }
}