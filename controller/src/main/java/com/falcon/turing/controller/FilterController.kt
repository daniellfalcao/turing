package com.falcon.turing.controller

class FilterController {

    // listagem de filtros ativos
    private val filters: MutableList<Pair<String, String>> = mutableListOf()

    // callback de finalizacao do filtro
    private var _onFilterFinished: () -> Unit = {}

    // callback de mudanca no filtro
    private var _onFilterChanged: (currentFilters: MutableList<Pair<String, String>>) -> Unit = {}

    // variavel de controle de ativacao do filtro
    var isFilterEnable: Boolean = false
        set(value) {

            field = value

            if (field) {
                _onFilterChanged(filters)
            } else {
                _onFilterFinished()
            }
        }

    /**
     * Funcao responsavel por setar o callback de finalizacao do filtro.
     *
     * @param event callback de finalizacao;
     * */
    fun setOnFilterFinished(event: () -> Unit) = apply { _onFilterFinished = event }

    /**
     * Funcao responsavel por setar o callback de mudanca no filtro.
     *
     * @param event callback de mudanca;
     * */
    fun setOnFilterChanged(event: (currentFilters: MutableList<Pair<String, String>>) -> Unit)  = apply {  _onFilterChanged = event }

    /**
     * Adiciona um novo filtro a lista de filtros ativos se a filtragem estiver ativa e notifica
     * que houve uma mudanca na lista de filtros.
     *
     * @param filter filtro que sera adicionado
     * */
    fun addFilter(filter: Pair<String, String>) = apply {

        if (isFilterEnable) {
            filters.add(filter)
            _onFilterChanged(filters)
        }
    }

    /**
     * Remove um filtro da lista de filtros ativos se a filtragem estiver ativa e notifica
     * que houve uma mudanca na lista de filtros.
     *
     * @param filter filtro que sera adicionado
     * */
    fun removeFilter(filter: Pair<String, String>) = apply {

        if (isFilterEnable) {
            filters.remove(filter)
            _onFilterChanged(filters)
        }
    }
}

