package com.falcon.turing.controller

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*

class SearchController {

    companion object {
        const val DEFAULT_SEARCH_VALUE = ""
    }

    sealed class SearchStatus {

        object Idle : SearchStatus()
        object NotFound : SearchStatus()
    }

    var lastQuery: String = DEFAULT_SEARCH_VALUE

    val searchStatus = MutableLiveData<SearchStatus>()
    var isSearchInProgress = false

    private var onSearch: (query: String) -> Unit = {}
    private var onSearchFinished: () -> Unit = {}
    private var searchCoroutine: Job? = null

    suspend fun search(query: String, searchStartTime: Long = 500) {

        val isQueryValid = query.trim().isNotEmpty()

        if (isQueryValid) {
            lastQuery = query.trim()
            isSearchInProgress = true

            searchCoroutine?.cancel()

            delay(searchStartTime)
            withContext(Dispatchers.Main) { onSearch(lastQuery) }

        } else {
            finishSearch()
        }
    }

    fun finishSearch() {

        // resetando variaveis de controle
        lastQuery = DEFAULT_SEARCH_VALUE
        isSearchInProgress = false

        // callback de busca finalizada
        onSearchFinished()
    }

    fun setOnSearch(event: (query: String) -> Unit) {
        onSearch = event
    }

    fun setOnSearchFinished(event: () -> Unit) {
        onSearchFinished = event
    }
}
